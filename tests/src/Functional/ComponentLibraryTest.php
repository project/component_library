<?php

declare(strict_types=1);

namespace Drupal\Tests\component_library\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test component library functionality.
 *
 * @group component_library
 */
final class ComponentLibraryTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'component_library',
    'entity',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Test component library.
   */
  public function testComponentLibrary(): void {
    $this->container->get('module_installer')->install(['layout_builder']);
    $assert = $this->assertSession();
    $permissions = [
      'administer component library variants',
      'administer component library patterns',
    ];
    $admin_user = $this->drupalCreateUser($permissions);
    $this->drupalLogin($admin_user);

    // -- Create component pattern.
    $this->drupalGet(Url::fromRoute('entity.component_library_pattern.collection'));
    $this->clickLink('Add library pattern');

    $edit = [
      'label' => 'Hero pattern',
      'id' => 'hero_pattern',
      'description' => 'Preview data for hero component',
      'data' => <<< 'YAML'
        html_id: 'random-header-id'
        color: red
        title: Fusce dui ipsum
        message: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at auctor tellus, eu mollis nunc.
        url: '#'
        button_text: Learn more
        YAML,
    ];
    $this->submitForm($edit, 'Save');
    $assert->responseContains('Created new component library pattern');

    // -- Create component variant.
    $this->drupalGet(Url::fromRoute('entity.component_library_variant.collection'));

    $assert->elementExists('xpath', '//h1[text() = "Component Library Variants"]');
    $this->clickLink('Add component variant');

    $edit = [
      'label' => 'Default Hero Component',
      'id' => 'default_hero_component',
      'template' => <<< 'TWIG'
        <header id="{{ html_id }}" role="banner" style="background-color: {{ color }};">
          <h1>{{ title }}</h1>
          <p>{{ message }}</p>
          <a href="{{ url }}" class="btn" target="_blank" rel="noopener noreferrer">{{ button_text }}</a>
        </header>
        TWIG,
      'pattern' => 'hero_pattern',
      'status' => TRUE,
    ];
    $this->submitForm($edit, 'Save');
    $assert->responseContains('New component variation');

    $this->clickLink('Default Hero Component');
    $text = <<< 'HTML'
      <header id="random-header-id" role="banner" style="background-color: red;">
        <h1>Fusce dui ipsum</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at auctor tellus, eu mollis nunc.</p>
        <a href="#" class="btn" target="_blank" rel="noopener noreferrer">Learn more</a>
      </header>
      HTML;
    $assert->responseContains($text);

    // -- Create a second component variant.
    $this->drupalGet(Url::fromRoute('entity.component_library_variant.collection'));
    $this->clickLink('Add component variant');

    $edit = [
      'label' => 'Secondary Hero Component',
      'id' => 'secondary_hero_component',
      'template' => <<< 'TWIG'
        {{ component_library_variant('hero_pattern__default_hero_component', _context) }}
        TWIG,
      'pattern' => 'hero_pattern',
      'status' => TRUE,
    ];
    $this->submitForm($edit, 'Save');
    $assert->responseContains('New component variation');

    $this->clickLink('Secondary Hero Component');

    $text = <<< 'HTML'
      <header id="random-header-id" role="banner" style="background-color: red;">
        <h1>Fusce dui ipsum</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at auctor tellus, eu mollis nunc.</p>
        <a href="#" class="btn" target="_blank" rel="noopener noreferrer">Learn more</a>
      </header>
      HTML;
    $assert->responseContains($text);
  }

  /**
   * Test component library.
   */
  public function testPatternValidation(): void {
    $assert = $this->assertSession();
    $permissions = [
      'administer component library patterns',
    ];
    $admin_user = $this->drupalCreateUser($permissions);
    $this->drupalLogin($admin_user);

    $this->drupalGet(Url::fromRoute('entity.component_library_pattern.collection'));
    $this->clickLink('Add library pattern');

    // -- Yaml syntax.
    $edit = [
      'label' => 'Example',
      'id' => 'example',
      'data' => '%%% invalid Yaml %%%',
    ];
    $this->submitForm($edit, 'Save');
    $assert->responseContains('This value should be valid YAML.');

    // -- Top-level array.
    $edit = [
      'label' => 'Example',
      'id' => 'example',
      'data' => 'A string',
    ];
    $this->submitForm($edit, 'Save');
    $assert->responseContains('The preview data should be defined as array.');

    // -- Keywords.
    $edit = [
      'label' => 'Example',
      'id' => 'example',
      'data' => 'type: tests',
    ];
    $this->submitForm($edit, 'Save');
    $assert->responseContains('The name "type" is a reserved keyword and cannot be used as Twig variable.');
  }

}
