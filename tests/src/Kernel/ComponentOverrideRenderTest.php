<?php

declare(strict_types=1);

namespace Drupal\Tests\component_library\Kernel;

use Drupal\component_library\Entity\ComponentOverride;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\user\Entity\User;

/**
 * Test component override rendering.
 *
 * @group component_library
 */
final class ComponentOverrideRenderTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'text',
    'block',
    'entity',
    'layout_builder',
    'component_library',
  ];

  private EntityViewBuilderInterface $viewBuilder;
  private RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->viewBuilder = $this->entityTypeManager->getViewBuilder('node');
    $this->renderer = $this->container->get('renderer');

    $type = NodeType::create([
      'type' => 'article',
      'name' => 'Article',
    ]);
    $type->save();

    $this->installSchema('node', 'node_access');
    $this->installConfig(['system', 'node']);

  }

  /**
   * Tests the rendering of component overrides.
   */
  public function testComponentOverrideRender(): void {
    $account = User::create([
      'name' => $this->randomString(),
    ]);
    $account->save();

    $node = Node::create([
      'type' => 'article',
      'title' => 'Article',
      'uid' => $account->id(),
    ]);
    $node->save();

    $override = ComponentOverride::create([
      'id' => 'article',
      'label' => 'Article',
      'override' => 'node',
      'theme' => 'core',
      'template' => 'Article template overridden!!!',
      'plugin' => 'content_entity:node',
      'plugin_data' => '{"bundle":"article","view_mode":"default"}',
      'variables' => '',
      'libraries' => [],
    ]);
    $override->save();
    $build = $this->viewBuilder->view($node, 'default');
    $output = (string) $this->renderer->renderRoot($build);
    self::assertStringContainsString('Article template overridden!!!', $output);
  }

}
