<?php

declare(strict_types=1);

namespace Drupal\Tests\component_library\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test component library asset functionality.
 *
 * @group component_library
 */
abstract class ComponentLibraryTestBase extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'text',
    'node',
    'entity',
    'toolbar',
    'layout_builder',
    'component_library',
  ];

  /**
   * Permissions for the admin user.
   *
   * @var array|string[]
   */
  protected array $permissions = [
    'administer component overrides',
    'administer themes',
    'edit any page content',
    'access override mode',
    'administer override mode',
    'access toolbar',
    'administer component library assets',
  ];

  /**
   * Node ID.
   */
  protected string $nid;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
    ]);

    $node = $this->drupalCreateNode([
      'title' => $this->randomMachineName(),
      'type' => 'page',
    ]);
    $this->nid = $node->id();
    $admin_user = $this->drupalCreateUser($this->permissions);
    $this->drupalLogin($admin_user);
  }

  /**
   * Fill code mirror editor.
   *
   * @param string $text
   *   The text area value.
   * @param string $form_element_selector
   *   The selector of the codemirror editor field.
   *
   * @throws \Behat\Mink\Exception\DriverException
   * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
   */
  protected function fillCodeMirrorEditor(string $text, string $form_element_selector = '.js-form-item-template'): void {
    $code = 'document.querySelector(\'' . $form_element_selector . ' .CodeMirror\').CodeMirror.setValue(\'' . $text . '\');';
    $this->getSession()->getDriver()->executeScript('document.querySelector(\'' . $form_element_selector . ' .CodeMirror\').CodeMirror.setValue(\'' . $text . '\');');
  }

}
