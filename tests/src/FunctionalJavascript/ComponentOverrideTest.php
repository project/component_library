<?php

declare(strict_types=1);

namespace Drupal\Tests\component_library\FunctionalJavascript;

use Drupal\Core\Url;

/**
 * Test component override functionality.
 *
 * @group component_library
 */
final class ComponentOverrideTest extends ComponentLibraryTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * Tests the override mode UI.
   */
  public function testOverrideModeUi(): void {
    $this->getSession()->maximizeWindow();

    $assert = $this->assertSession();
    $this->drupalGet("node/{$this->nid}");
    $page = $this->getSession()->getPage();

    // Ensure override details for each DOM element were set.
    $drupal_settings = $this->getDrupalSettings();
    self::assertNotEmpty($drupal_settings['overrides']);

    // Ensure HTML override comments were converted into attributes.
    // @see override_mode.js processHtmlComments()
    self::assertNotEmpty($assert->waitForElement('css', '[data-override-mode]'));

    // Enable Override Mode.
    $assert->elementExists('css', '#enable-override-mode')->click();
    self::assertNotEmpty($assert->waitForElement('css', '#override-mode-tray'));

    // Test opening the Override form with a tray button.
    $assert->elementExists('css', '.override-button')->click();
    $assert->assertWaitOnAjaxRequest();
    self::assertNotEmpty($assert->waitForElementVisible('css', 'form.component-override-add-form'));
    // Clicking the close button wasn't working with normal methods. The dialog
    // buttons are consistently weird in the test environment.
    $this->getSession()->executeScript("jQuery('[aria-describedby=\"drupal-modal\"] .ui-dialog-titlebar-close').click()");
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Test clicking a DOM element to open the Override form.
    $assert->elementExists('css', 'h1')->click();
    $assert->assertWaitOnAjaxRequest();
    self::assertNotEmpty($assert->waitForElementVisible('css', 'form.component-override-add-form'));

    // Override the page title.
    $page->fillField('label', 'Page Title');
    $page_title_template = '<h3 id="overridden-page-title">Overridden page title!</h3>';
    $this->fillCodeMirrorEditor($page_title_template);

    // Test preview.
    // Not sure why we need to sleep here. Possibly related?
    // https://www.drupal.org/project/drupal/issues/2789381
    \sleep(1);
    $assert->elementExists('css', '.ui-dialog-buttonpane .preview-template')->click();
    $assert->assertWaitOnAjaxRequest();
    self::assertNotEmpty($assert->waitForElementVisible('css', '#override-preview-context'));
    $assert->elementContains('css', '#override-preview-context', $page_title_template);

    // Test override.
    \sleep(1);
    $assert->elementExists('css', '.ui-dialog-buttonpane .button--primary')->press();
    $assert->pageTextContains($page_title_template);

    // Enable Override Mode.
    $assert->elementExists('css', '#enable-override-mode')->click();
    $assert->assertWaitOnAjaxRequest();
    self::assertNotEmpty($assert->waitForElement('css', '#override-mode-tray'));
    $assert->assertWaitOnAjaxRequest();

    // Edit the page title override.
    $assert->elementExists('css', '#overridden-page-title')->click();
    $assert->assertWaitOnAjaxRequest();
    self::assertNotEmpty($assert->waitForElementVisible('css', 'form.component-override-edit-form'));

    // Ensure the plugin and override are disabled.
    $assert->elementAttributeExists('css', '[data-drupal-selector="edit-plugin-container-plugin"]', 'disabled');
    $assert->elementAttributeExists('css', '[data-drupal-selector="edit-plugin-container-override"]', 'disabled');
  }

  /**
   * Test the component override content entity plugin.
   */
  public function testComponentOverrideContentEntityPlugin(): void {
    $assert = $this->assertSession();
    // Ensure you can add a component override via the theme settings page.
    $theme_settings_page = Url::fromRoute('system.theme_settings_theme', ['theme' => 'stable']);
    $this->drupalGet($theme_settings_page);
    $assert->linkExists('Add component override');
    $this->clickLink('Add component override');
    $assert->elementExists('css', 'form.component-override-form');

    // Create a component override for a page node.
    $page = $this->getSession()->getPage();
    $page->fillField('label', 'Article');
    $page->selectFieldOption('plugin_container[plugin]', 'content_entity:node');
    $assert->assertWaitOnAjaxRequest();
    self::assertNotEmpty($assert->waitForElementVisible('css', '[name="plugin_container[view_mode]"]'));
    $page->selectFieldOption('plugin_container[view_mode]', 'default');
    $assert->assertWaitOnAjaxRequest();
    self::assertNotEmpty($assert->waitForElementVisible('css', '[name="plugin_container[override]"]'));
    $page->selectFieldOption('plugin_container[override]', 'node__page');
    $assert->assertWaitOnAjaxRequest();
    // Ensure the default template was loaded.
    $assert->pageTextContains('Theme override to display a node.');
    // Override the template.
    $this->fillCodeMirrorEditor('Page template overridden!!!');
    $button = $assert->elementExists('css', '#edit-submit');
    $button->press();
    $assert->assertWaitOnAjaxRequest();

    // Ensure the theme settings page lists overrides.
    $this->drupalGet($theme_settings_page);
    $assert->pageTextContains('Article');
    $assert->pageTextContains('content_entity:node');
    $assert->pageTextContains('node__page');

    // Ensure the template was overridden.
    $this->drupalGet("node/{$this->nid}");
    $assert->pageTextContains('Page template overridden!!!');
  }

}
