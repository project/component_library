# Component Library

---------------

## About this Module

Component library is a way to do theming as a site-building task.

### Installing the Component LibraryModule

Note: Use of the module on a Composer managed site is also supported.

1. Copy/upload the module to the modules' directory of your Drupal installation.
2. Enable the module in 'Extend' (/admin/modules).
3. Configure the module to define new patterns.
  (/admin/structure/component-library-pattern).
4. Then create variations for the pattern.
5. The module as an API does not require any specific component system's module.
   However, if installed, `ui_patterns` integration is available.

The module works particularly well with any CSS utility frameworks like
TailWind or Bootstrap. Build the markup and variations, use UI Patterns
(or similar) to then use the patterns and variations.
