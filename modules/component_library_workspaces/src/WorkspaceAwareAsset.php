<?php

declare(strict_types=1);

namespace Drupal\component_library_workspaces;

use Drupal\component_library\Asset;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\component_library\Entity\ComponentLibraryAsset;
use Drupal\workspaces\WorkspaceManagerInterface;

/**
 * Asset file handler service.
 */
final class WorkspaceAwareAsset extends Asset {

  /**
   * The decorated service.
   *
   * @var \Drupal\component_library\Asset
   */
  protected $inner;

  /**
   * The workspace manager service.
   *
   * @var \Drupal\workspaces\WorkspaceManagerInterface
   */
  protected $workspaceManager;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  public function __construct(Asset $inner, FileSystemInterface $file_system, ConfigFactoryInterface $config_factory, WorkspaceManagerInterface $workspace_manager, CacheTagsInvalidatorInterface $cachetags_invalidator) {
    $this->inner = $inner;
    $this->fileSystem = $file_system;
    $this->configFactory = $config_factory;
    $this->workspaceManager = $workspace_manager;
    $this->cacheTagsInvalidator = $cachetags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public function generateLibraryFiles(ComponentLibraryAsset $asset) {
    $this->inner->generateLibraryFiles($asset);
  }

  /**
   * Clear files.
   *
   * Deletes existing asset files with or without aggregation enabled.
   *
   * @param \Drupal\component_library\Entity\ComponentLibraryAsset $asset
   *   The component_library_asset to clean up files for.
   */
  public function clearFiles(ComponentLibraryAsset $asset): void {
    $this->cacheTagsInvalidator->invalidateTags(['library_info']);
    foreach (['js', 'css'] as $asset_type) {
      $aggregation_enabled = $this->configFactory->get('system.performance')->get("$asset_type.preprocess");
      if ($aggregation_enabled) {
        $this->fileSystem->deleteRecursive('public://' . $asset_type . '/');
      }
    }
    $workspace = $this->workspaceManager->getActiveWorkspace();
    $path = 'public://component_library_assets/' . $asset->id() . '/';
    $path .= $workspace ? $workspace->id() : 'live';
    $this->fileSystem->deleteRecursive($path);
  }

}
