<?php

declare(strict_types=1);

namespace Drupal\component_library_workspaces;

use Drupal\component_library_workspaces\EventSubscriber\WorkspacePublishingSubscriber;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Defines a service provider for the Component Library Workspaces module.
 */
final class ComponentLibraryWorkspacesServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $modules = $container->getParameter('container.modules');
    if (\array_key_exists('wse_config', $modules)) {
      $container->register('component_library_workspaces.workspace_publishing_subscriber', WorkspacePublishingSubscriber::class)
        ->addTag('event_subscriber')
        ->addArgument(new Reference('entity_type.manager'))
        ->addArgument(new Reference('file_system'))
        ->addArgument(new Reference('workspaces.manager'))
        ->addArgument(new Reference('workspaces.association'))
        ->addArgument(new Reference('config.storage.active'))
        ->addArgument(new Reference('state'))
        ->addArgument(new Reference('config.factory'))
        ->addArgument(new Reference('library.discovery'));
    }
  }

}
