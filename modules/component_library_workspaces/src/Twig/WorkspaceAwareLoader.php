<?php

declare(strict_types=1);

namespace Drupal\component_library_workspaces\Twig;

use Drupal\component_library\Entity\ComponentOverride;
use Drupal\workspaces\WorkspaceManagerInterface;
use Twig\Loader\LoaderInterface;
use Twig\Source;

/**
 * Component Override twig template loader.
 */
class WorkspaceAwareLoader implements LoaderInterface {

  /**
   * The workspace manager service.
   *
   * @var \Drupal\workspaces\WorkspaceManagerInterface
   */
  protected $workspaceManager;

  /**
   * The decorated service.
   *
   * @var \Twig\Loader\LoaderInterface
   */
  protected $inner;

  public function __construct(LoaderInterface $inner, WorkspaceManagerInterface $workspace_manager) {
    $this->inner = $inner;
    $this->workspaceManager = $workspace_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheKey($name): string {
    $workspace = \Drupal::service('workspaces.manager')->getActiveWorkspace();
    return $workspace ? $name . '__' . $workspace->id() : $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceContext($name): Source {
    return $this->inner->getSourceContext($name);
  }

  /**
   * {@inheritdoc}
   */
  public function isFresh($name, $time): bool {
    return $this->inner->isFresh($name, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function exists($name): bool {
    return $this->inner->exists($name);
  }

  /**
   * Get Override by name.
   *
   * @param string $name
   *   The full template path and filename.
   *
   * @return \Drupal\component_library\Entity\ComponentOverride|null
   *   The override.
   */
  public function getOverrideByName(string $name): ?ComponentOverride {
    return $this->inner->getOverrideByName($name);
  }

}
