<?php

namespace Drupal\component_library_workspaces\EventSubscriber;

use Drupal\component_library\Event\GetAssetFilesEvent;
use Drupal\workspaces\WorkspaceManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber for the event that is dispatched when asset files are read.
 */
class GetAssetFilesSubscriber implements EventSubscriberInterface {

  /**
   * The workspace manager service.
   *
   * @var \Drupal\workspaces\WorkspaceManagerInterface
   */
  protected $workspaceManager;

  /**
   * Constructs a new GetAssetFilesSubscriber.
   *
   * @param \Drupal\workspaces\WorkspaceManagerInterface $workspace_manager
   *   The workspace manager service.
   */
  public function __construct(WorkspaceManagerInterface $workspace_manager) {
    $this->workspaceManager = $workspace_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      GetAssetFilesEvent::class => ['onGetAssetFiles'],
    ];
  }

  /**
   * Alters asset file paths to workspace specific paths.
   *
   * @param \Drupal\component_library\Event\GetAssetFilesEvent $event
   *   The workspace publish event.
   */
  public function onGetAssetFiles(GetAssetFilesEvent $event) {
    $workspace = $this->workspaceManager->getActiveWorkspace();
    foreach ($event->getFiles() as &$file) {
      $asset_id = $event->getAsset()->id();
      $code_aware_name = md5($file['code'] . $asset_id);
      $workspace_id = $workspace ? $workspace->id() : 'live';
      $file['path'] = sprintf('public://component_library_assets/%s/%s/%s/%s.%s', $asset_id, $workspace_id, $file['type'], $code_aware_name, $file['type']);

      // Only fall back to asset file paths of live workspace for read access.
      // In case this is called to generate files inside a workspace, we can't
      // fall back to the live workspace path because we don't want to override
      // the asset files of live.
      if (!$event->getIsCreating() && !file_exists($file['path'])) {
        $file['path'] = sprintf('public://component_library_assets/%s/%s/%s/%s.%s', $asset_id, 'live', $file['type'], $code_aware_name, $file['type']);
      }
    }
  }

}
