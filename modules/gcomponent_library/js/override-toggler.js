/**
 * @file
 * Override toggler behaviors.
 */

(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.uhSiteOverrideToggler = {
    attach: function (context) {

      once('gcomponent_library.override-container', '[data-style-override-container]', context).forEach(
        container => {
          const toggler = container.querySelector('[data-style-override-toggler]');
          const input = container.querySelector('[data-style-override-input]');
          container.classList.add('style-override-container');
          if (input.type === 'checkbox') {
            container.classList.add('style-override-container_checkbox');
          }
          input.disabled = !toggler.checked;
          toggler.addEventListener('change', () => input.disabled = !toggler.checked);
        }
      )

    }
  };

} (Drupal, once));
