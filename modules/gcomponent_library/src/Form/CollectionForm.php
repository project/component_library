<?php

declare(strict_types=1);

namespace Drupal\gcomponent_library\Form;

use Drupal\component_library\Entity\ComponentLibraryPattern;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\gcomponent_library\Entity\LibraryCollection;

/**
 * Collection form.
 *
 * @property \Drupal\gcomponent_library\Entity\LibraryCollection $entity
 */
final class CollectionForm extends EntityForm {

  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the library collection.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => \sprintf('%s::load', LibraryCollection::class),
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#rows' => 1,
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the library collection.'),
    ];

    $form['styles'] = [
      '#type' => 'details',
      '#title' => $this->t('Styles'),
      '#tree' => TRUE,
    ];
    foreach (LibraryCollection::getSupportedStyles() as $key => $label) {
      $pattern = $this->entityTypeManager
        ->getStorage('component_library_pattern')
        ->load($key);
      \assert($pattern instanceof ComponentLibraryPattern);
      $tags = $pattern->get('tags');
      $tag = !empty($tags) ? (string) \reset($tags) : (string) $this->t('Default');
      $form['styles'][$tag][$key] = [
        '#type' => 'select',
        '#title' => $label,
        '#options' => [],
        '#required' => TRUE,
        '#default_value' => $this->entity->getStyle($key),
      ];

      foreach ($pattern->getVariants() as $variation) {
        $form['styles'][$tag][$key]['#options'][$variation->id()] = $variation->label();
      }
      \asort($form['styles'][$tag][$key]['#options'], \SORT_NATURAL);
    }
    $children = Element::children($form['styles']);
    foreach ($children as $child) {
      $value =& $form['styles'][$child];
      $value['#type'] = 'details';
      $value['#title'] = $child;
    }
    if ($children === []) {
      $form['styles']['#description'] = $this->t('<a href="@url">Setup site overridable</a> styles first.', [
        '@url' => Url::fromRoute('gcomponent_library.settings')->toString(),
      ]);
    }

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state): void {
    $styles = $form_state->getValue('styles');
    // ::afterBuild and ::submitForm invoke this method, only convert once.
    if (\array_key_exists('Content', $styles)) {
      $formatted_styles = \call_user_func_array('array_merge', $styles);
      \ksort($formatted_styles);
      $form_state->setValue('styles', $formatted_styles);
    }

    parent::copyFormValuesToEntity($entity, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result === \SAVED_NEW
      ? $this->t('Created new library collection %label.', $message_args)
      : $this->t('Updated library collection %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
