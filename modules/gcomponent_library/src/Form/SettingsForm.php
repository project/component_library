<?php

declare(strict_types=1);

namespace Drupal\gcomponent_library\Form;

use Drupal\component_library\Entity\ComponentLibraryPattern;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Group Component Library settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  private ConfigEntityStorageInterface $patternStorage;

  public static function create(ContainerInterface $container): self {
    $instance = new static(
      $container->get('config.factory'),
    );
    $instance->patternStorage = $container->get('entity_type.manager')
      ->getStorage('component_library_pattern');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'gcomponent_library_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['gcomponent_library.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $patterns = $this->patternStorage->loadMultiple();
    $form['override_patterns'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Group overridable patterns'),
      '#options' => \array_map(static fn (ComponentLibraryPattern $pattern) => $pattern->label(), $patterns),
      '#default_value' => $this->config('gcomponent_library.settings')->get('override_patterns') ?? [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('gcomponent_library.settings')
      ->set('override_patterns', $form_state->getValue('override_patterns'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
