<?php

declare(strict_types=1);

namespace Drupal\gcomponent_library;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

final class BaseFields {

  public const COLLECTION_FIELD = 'library_collection';
  public const OVERRIDE_FIELD = 'style_overrides';

  /**
   * Return the base field info.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface[]
   *   The field storage definitions.
   */
  public function baseFieldInfo(EntityTypeInterface $entity_type): array {
    $fields = [];

    if ($entity_type->id() === 'group') {
      $fields[self::COLLECTION_FIELD] = BaseFieldDefinition::create('entity_reference')
        ->setName('Component library collection')
        ->setTargetEntityTypeId($entity_type->id())
        ->setTranslatable(FALSE)
        ->setRevisionable(FALSE)
        ->setCardinality(1)
        ->setSetting('target_type', 'gcomponent_library_collection')
        ->setLabel(new TranslatableMarkup('Component library collection'))
        ->setDisplayConfigurable('form', TRUE)
        ->setDescription(new TranslatableMarkup('The collection of related design components.'))
        ->setRequired(TRUE);
      $fields[self::OVERRIDE_FIELD] = BaseFieldDefinition::create('map')
        ->setLabel(new TranslatableMarkup('Style overrides'))
        ->setDisplayOptions('form', [
          'type' => 'gcomponent_library_style_overrides',
        ])
        ->setDisplayConfigurable('form', TRUE);
    }

    return $fields;
  }

}
