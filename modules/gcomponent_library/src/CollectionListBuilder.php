<?php

declare(strict_types=1);

namespace Drupal\gcomponent_library;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of library collections.
 */
final class CollectionListBuilder extends ConfigEntityListBuilder {

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   */
  public function buildHeader(): array {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * @return array<string, mixed>
   */
  public function buildRow(EntityInterface $entity): array {
    $row = [];
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

}
