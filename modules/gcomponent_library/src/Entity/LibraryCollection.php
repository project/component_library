<?php

declare(strict_types=1);

namespace Drupal\gcomponent_library\Entity;

use Drupal\component_library\Entity\ComponentLibraryPattern;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the library collection entity type.
 *
 * @ConfigEntityType(
 *   id = "gcomponent_library_collection",
 *   label = @Translation("Library collection"),
 *   label_collection = @Translation("Library collections"),
 *   label_singular = @Translation("library collection"),
 *   label_plural = @Translation("library collections"),
 *   label_count = @PluralTranslation(
 *     singular = "@count library collection",
 *     plural = "@count library collections",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\gcomponent_library\CollectionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\gcomponent_library\Form\CollectionForm",
 *       "edit" = "Drupal\gcomponent_library\Form\CollectionForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "default" = "\Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *   },
 *   config_prefix = "collection",
 *   admin_permission = "administer component library collection",
 *   links = {
 *     "collection" = "/admin/structure/component-library/collections",
 *     "add-form" = "/admin/structure/component-library/collection/add",
 *     "edit-form" = "/admin/structure/component-library/collection/{gcomponent_library_collection}",
 *     "delete-form" = "/admin/structure/component-library/collection/{gcomponent_library_collection}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "weight",
 *     "styles",
 *   },
 * )
 */
final class LibraryCollection extends ConfigEntityBase {

  /**
   * The collection ID.
   */
  protected string $id;

  /**
   * The collection label.
   */
  protected string $label;

  /**
   * The collection description.
   */
  protected string $description;

  /**
   * The collection weight.
   */
  protected int $weight = 0;

  /**
   * The styles.
   */
  protected array $styles = [];

  /**
   * Gets supported styles.
   *
   * @return string[]
   */
  public static function getSupportedStyles(): array {
    $patterns = \Drupal::config('gcomponent_library.settings')
      ->get('override_patterns');
    $styles = $patterns !== NULL ? \Drupal::entityTypeManager()->getStorage('component_library_pattern')
      ->loadMultiple($patterns) : [];
    return \array_map(static fn (ComponentLibraryPattern $pattern) => $pattern->label(), $styles);
  }

  public function getStyle(string $name): ?string {
    return $this->get('styles')[$name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): self {
    parent::calculateDependencies();

    foreach (\array_keys($this->styles) as $style) {
      $style_item = $this->entityTypeManager()
        ->getStorage('component_library_variant')
        ->load($this->getStyle($style));
      if ($style_item) {
        $this->addDependency('config', $style_item->getConfigDependencyName());
      }
    }

    return $this;
  }

}
