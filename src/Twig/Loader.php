<?php

declare(strict_types=1);

namespace Drupal\component_library\Twig;

use Drupal\component_library\Entity\ComponentOverride;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Twig\Error\LoaderError;
use Twig\Loader\LoaderInterface;
use Twig\Source;

/**
 * Component Override twig template loader.
 */
class Loader implements LoaderInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  private EntityStorageInterface $overrides;
  private ThemeManagerInterface $themeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, ThemeManagerInterface $theme_manager) {
    $this->overrides = $entityTypeManager->getStorage('component_override');
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceContext($name): Source {
    $override = $this->getOverrideByName($name);
    if ($override) {
      return new Source($override->get('template'), $name);
    }

    throw new LoaderError($name);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheKey($name): string {
    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function isFresh($name, $time): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function exists($name): bool {
    return (bool) $this->getOverrideByName($name);
  }

  /**
   * Get Override by name.
   *
   * @param string $name
   *   The full template path and filename.
   *
   * @return \Drupal\component_library\Entity\ComponentOverride|null
   *   The override.
   */
  public function getOverrideByName(string $name): ?ComponentOverride {
    $override = NULL;
    $overrides = NULL;
    $override_name = $this->getOverrideNameFromName($name);
    $theme = $this->themeManager->getActiveTheme();
    if ($theme) {
      $overrides = $this->overrides->loadByProperties([
        'override' => $override_name,
        'theme' => $theme->getName(),
      ]);
    }
    if ($overrides) {
      $override = \reset($overrides);
    }
    return $override;
  }

  /**
   * Get override name from name.
   *
   * @param string $name
   *   The template path.
   *
   * @return string
   *   A theme registry compatible name.
   */
  private function getOverrideNameFromName(string $name): string {
    $parts = \explode('/', $name);
    $override = \array_pop($parts);
    $pos = \strpos($override, '.');
    $pos = $pos === FALSE ? 0 : $pos;
    $override = \substr($override, 0, $pos);
    return \str_replace('-', '_', $override);
  }

}
