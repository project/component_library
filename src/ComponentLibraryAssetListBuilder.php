<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of component_library_asset config entities.
 */
final class ComponentLibraryAssetListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\component_library\ComponentLibraryAssetInterface $entity */
    $row = [];
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    return $row + parent::buildRow($entity);
  }

  public function getDefaultOperations(EntityInterface $entity) {
    $options = parent::getDefaultOperations($entity);
    $edit_options = $options['edit']['url']->getOptions();
    unset($edit_options['query']['destination']);
    $options['edit']['url']->setOptions($edit_options);
    return $options;
  }


}
