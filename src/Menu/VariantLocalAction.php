<?php

declare(strict_types=1);

namespace Drupal\component_library\Menu;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Modifies the 'Add variant' local action to add options.
 */
final class VariantLocalAction extends LocalActionDefault {

  private RouteMatchInterface $currentRoute;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteProviderInterface $route_provider, RouteMatchInterface $current_route) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);

    $this->currentRoute = $current_route;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('router.route_provider'),
      $container->get('current_route_match'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(RouteMatchInterface $route_match) {
    $options = parent::getOptions($route_match);
    // Append the current path as destination to the query string.
    $options['query']['destination'] = Url::fromRoute('<current>')->toString();
    $pattern = $this->currentRoute->getParameter('component_library_pattern');
    if ($pattern !== NULL) {
      $options['query']['pattern'] = $pattern->id();
    }
    return $options;
  }

}
