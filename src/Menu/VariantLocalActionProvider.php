<?php

declare(strict_types=1);

namespace Drupal\component_library\Menu;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity\Menu\EntityLocalActionProviderInterface;

final class VariantLocalActionProvider implements EntityLocalActionProviderInterface {

  use StringTranslationTrait;

  public function buildLocalActions(EntityTypeInterface $entity_type): array {
    $actions = [];

    $actions['entity.component_library_variant.add'] = [
      'title' => $this->t('Add variant'),
      'route_name' => 'entity.component_library_variant.add_form',
      'class' => VariantLocalAction::class,
      // Every one of the links will be different, no need to cache what could
      // be many local action tasks that are all unique.
      'cache_max_age' => 0,
      'appears_on' => ['entity.component_library_pattern.edit_form'],
    ];

    return $actions;
  }

}
