<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\component_library\Entity\ComponentLibraryVariant;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension.
 */
final class ComponentLibraryTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('component_library_variant', [self::class, 'variant']),
    ];
  }

  /**
   * Returns the render array for a component library variant.
   */
  public static function variant(string $name, array $arguments = []): array {
    /** @var \Drupal\component_library\Entity\ComponentLibraryVariant $variant */
    $variant = \Drupal::entityTypeManager()->getStorage('component_library_variant')->load($name);
    if (!$variant instanceof ComponentLibraryVariant) {
      return [];
    }
    return [
      '#type' => 'inline_template',
      '#template' => $variant->get('template'),
      '#context' => $arguments,
    ];
  }

}
