<?php

declare(strict_types=1);

namespace Drupal\component_library\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Component Override type annotation.
 *
 * @Annotation
 */
final class ComponentOverride extends Plugin {}
