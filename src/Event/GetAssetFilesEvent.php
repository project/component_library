<?php

declare(strict_types=1);

namespace Drupal\component_library\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\component_library\Entity\ComponentLibraryAsset;

/**
 * Add placeholders to template variables.
 */
final class GetAssetFilesEvent extends Event {

  /**
   * The asset the files are being requested for.
   *
   * @var \Drupal\component_library\Entity\ComponentLibraryAsset
   */
  private ComponentLibraryAsset $asset;

  /**
   * The asset's files.
   *
   * @var \Drupal\component_library\Entity\ComponentLibraryAsset
   */
  private array $files;

  /**
   * Indicated whether the requested file data is used for writing files.
   *
   * @var \Drupal\component_library\Entity\ComponentLibraryAsset
   */
  private bool $isCreating;

  public function __construct(ComponentLibraryAsset $asset, array $files, bool $create) {
    $this->asset = $asset;
    $this->files = $files;
    $this->isCreating = $create;
  }

  public function getAsset(): ComponentLibraryAsset {
    return $this->asset;
  }

  public function getFiles(): array {
    return $this->files;
  }

  public function setFiles(array $files): void {
    $this->files = $files;
  }

  public function getIsCreating(): bool {
    return $this->isCreating;
  }

}
