<?php

declare(strict_types=1);

namespace Drupal\component_library\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Add placeholders to template variables.
 */
final class AddVariablesPlaceholderEvent extends Event {

  /**
   * @var mixed
   *   The item that may need a placeholder.
   */
  private $item;

  /**
   * @var array
   *  The #parents of the item.
   */
  private array $parents;

  /**
   * @var array
   *   The template variables.
   */
  private array $variables;

  /**
   * @var array|null
   *   A placeholder that will be dispatched in ReplaceVariablesPlaceholderEvent
   *   in the format #override_mode => [#type => type_of_placeholder].
   */
  private ?array $placeHolder;

  public function __construct($item, array $parents, array $variables) {
    $this->item = $item;
    $this->parents = $parents;
    $this->variables = $variables;
    $this->placeHolder = NULL;
  }

  public function getItem() {
    return $this->item;
  }

  /**
   * @return array
   */
  public function getParents(): array {
    return $this->parents;
  }

  /**
   * @return array
   */
  public function getVariables(): array {
    return $this->variables;
  }

  /**
   * Set placeHolder.
   *
   * @param array $placeHolder
   *   The placeholder.
   */
  public function setPlaceHolder(array $placeHolder): void {
    $this->placeHolder = $placeHolder;
  }

  /**
   * Get placeHolder.
   *
   * @return mixed|null
   *   The placeholder.
   */
  public function getPlaceHolder() {
    return $this->placeHolder;
  }

}
