<?php

declare(strict_types=1);

namespace Drupal\component_library\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\component_library\Entity\ComponentOverride;
use Drupal\Core\TempStore\PrivateTempStore;

/**
 * Prepare override event.
 */
final class PrepareOverrideEvent extends Event {

  /**
   * The tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   *   The variables cache.
   */
  private PrivateTempStore $tempStore;

  /**
   * A list of theme suggestions available for overriding.
   *
   * @var array
   */
  private array $overrideOptions = [];

  /**
   * Prepopulated.
   *
   * @var bool
   *   Flags whether or not the Override was prepopulated.
   */
  private bool $prepopulated;

  /**
   * @var string|null
   *   The component override plugin ID.
   */
  private ?string $plugin = NULL;

  /**
   * @var string|null
   *   The theme_suggestion to override.
   */
  private ?string $override = NULL;

  /**
   * @var array|null
   *   An array of component override plugin data.
   */
  private ?array $pluginData = NULL;

  /**
   * @var \Drupal\component_library\Entity\ComponentOverride|null
   *   The override.
   */
  private ?ComponentOverride $entity;

  public function __construct() {
    $this->prepopulated = FALSE;
    $this->tempStore = \Drupal::service('tempstore.private')->get('component_overrides');
  }

  /**
   * Get variables.
   *
   * @return array|null
   *   The preprocessed variables used to render the template.
   */
  public function getVariables(): ?array {
    return $this->tempStore->get('variables');
  }

  /**
   * Set variables.
   *
   * @param array $variables
   *   The preprocessed variables used to render the template.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function setVariables(array $variables): void {
    $this->tempStore->set('variables', $variables);
  }

  /**
   * Get override options.
   *
   * @return array
   *   The override select list options.
   */
  public function getOverrideOptions(): array {
    return $this->overrideOptions;
  }

  /**
   * Set override options.
   *
   * @param array $override_options
   *   An array of available theme suggestions to override for this template.
   */
  public function setOverrideOptions(array $override_options): void {
    $this->overrideOptions = $override_options;
  }

  /**
   * Flag that this override form has been prepopulated.
   */
  public function prepopulated(): void {
    $this->prepopulated = TRUE;
  }

  /**
   * Is prepopulated.
   *
   * @return bool
   *   Whether or not the override form has been prepopulated.
   */
  public function isPrepopulated(): bool {
    return $this->prepopulated;
  }

  /**
   * Get Plugin.
   *
   * @return string
   *   The component override plugin ID.
   */
  public function getPlugin(): ?string {
    return $this->plugin;
  }

  /**
   * Set plugin.
   *
   * @param string $plugin
   *   The component override plugin ID.
   */
  public function setPlugin(string $plugin): void {
    $this->plugin = $plugin;
  }

  /**
   * Get override.
   *
   * @return string|null
   *   The theme_suggestion to override.
   */
  public function getOverride(): ?string {
    return $this->override;
  }

  /**
   * Set override.
   *
   * @param string|null $override
   *   The theme_suggestion to override.
   */
  public function setOverride(?string $override): void {
    $this->override = $override;
  }

  /**
   * Get.
   *
   * @param string $key
   *   The property to retrieve.
   *
   * @return mixed|null
   *   The property or NULL.
   */
  public function get(string $key) {
    return $this->pluginData[$key] ?? $this->{$key} ?? NULL;
  }

  /**
   * Get plugin data.
   *
   * @return array|null
   *   The plugin data for this override.
   */
  public function getPluginData(): ?array {
    return $this->pluginData;
  }

  /**
   * Set plugin data.
   *
   * @param array|null $plugin_data
   *   The plugin data for this override.
   */
  public function setPluginData(?array $plugin_data): void {
    $this->pluginData = $plugin_data;
  }

  /**
   * Get entity.
   *
   * @return \Drupal\component_library\Entity\ComponentOverride|null
   *   The component override.
   */
  public function getEntity(): ?ComponentOverride {
    return $this->entity;
  }

  /**
   * Set entity.
   *
   * @param \Drupal\component_library\Entity\ComponentOverride|null $entity
   *   The component override.
   */
  public function setEntity(?ComponentOverride $entity): void {
    $this->entity = $entity;
  }

}
