<?php

declare(strict_types=1);

namespace Drupal\component_library\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Replace placeholders in template variables.
 */
final class ReplaceVariablesPlaceholderEvent extends Event {

  /**
   * @var array
   *   The type of placeholder and additional arguments.
   */
  private array $config;

  /**
   * @var mixed
   *   The replaced item.
   */
  private $replacement;

  public function __construct(array $config) {
    $this->config = $config;
    $this->replacement = NULL;
  }

  /**
   * Get config.
   *
   * @return array
   *   The placeholder configuration.
   */
  public function getConfig(): array {
    return $this->config;
  }

  /**
   * Get replacement.
   *
   * @return mixed
   *   The item to replace the placeholder.
   */
  public function getReplacement() {
    return $this->replacement;
  }

  /**
   * Set replacement.
   *
   * @param mixed $replacement
   *   The item to replace the placeholder.
   */
  public function setReplacement($replacement): void {
    $this->replacement = $replacement;
  }

}
