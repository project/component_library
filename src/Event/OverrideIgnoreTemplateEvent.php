<?php

declare(strict_types=1);

namespace Drupal\component_library\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Override ignore template event.
 */
final class OverrideIgnoreTemplateEvent extends Event {

  /**
   * Template.
   *
   * @var string
   *   The path and file name of the template.
   */
  private string $template;

  /**
   * Variables.
   *
   * @var array
   *   The preprocessed variables to be rendered.
   */
  private array $variables;

  /**
   * Ignore.
   *
   * @var bool
   *   Whether or not to add override mode functionality to this template.
   */
  private bool $ignore;

  /**
   * Constructs an Override ignore template event.
   *
   * @param string $template
   *   The path and file name of the template.
   * @param array $variables
   *   The preprocessed variables to be rendered.
   */
  public function __construct(string $template, array $variables) {
    $this->template = $template;
    $this->variables = $variables;
    $this->ignore = FALSE;
  }

  /**
   * Template.
   *
   * @var string
   *   The path and file name of the template.
   */
  public function getTemplate(): string {
    return $this->template;
  }

  /**
   * Variables.
   *
   * @var array
   *   The preprocessed variables to be rendered.
   */
  public function getVariables(): array {
    return $this->variables;
  }

  /**
   * Is ignored.
   *
   * @return bool
   *   Whether to ignore this template.
   */
  public function isIgnored(): bool {
    return $this->ignore;
  }

  /**
   * Ignore this template.
   */
  public function ignore(): void {
    $this->ignore = TRUE;
  }

}
