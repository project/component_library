<?php

declare(strict_types=1);

namespace Drupal\component_library\Event;

use Symfony\Contracts\EventDispatcher\Event;

final class ConvertYamlPlaceholdersEvent extends Event {

  private array $context;
  private string $viewMode;

  public function __construct(array $context, ?string $view_mode = 'default') {
    $this->context = $context;
    $this->viewMode = $view_mode;
  }

  public function getContext(): array {
    return $this->context;
  }

  public function setContext(array $context): void {
    $this->context = $context;
  }

  public function setContextItem($key, $value): void {
    $this->context[$key] = $value;
  }

  public function getViewMode(): string {
    return $this->viewMode;
  }

}
