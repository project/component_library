<?php

declare(strict_types=1);

namespace Drupal\component_library\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class ComponentOverrideDeleteForm extends EntityDeleteForm {

  private ThemeHandlerInterface $themeHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self($container->get('theme_handler'));
  }

  /**
   * Constructs a new ComponentOverrideDeleteForm.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(ThemeHandlerInterface $theme_handler) {
    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('entity.component_override.collection', ['theme' => $this->themeHandler->getDefault()]);
  }

}
