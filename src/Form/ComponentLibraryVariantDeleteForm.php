<?php

declare(strict_types=1);

namespace Drupal\component_library\Form;

use Drupal\component_library\Entity\ComponentLibraryVariant;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Url;

/**
 * Provides a deletion form for variants.
 */
final class ComponentLibraryVariantDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return $this->getRedirectUrl();
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl(): Url {
    $entity = $this->getEntity();
    \assert($entity instanceof ComponentLibraryVariant);
    if ($pattern = $entity->getPatternEntity()) {
      return $pattern->toUrl();
    }
    return Url::fromRoute('entity.component_library_pattern.collection');
  }

}
