<?php

declare(strict_types=1);

namespace Drupal\component_library\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure Override Mode for this site.
 */
final class ComponentOverrideSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'component_override_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['component_library.override_mode.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('component_library.override_mode.settings');
    $form['override_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Override Mode'),
      '#default_value' => $config->get('override_mode'),
    ];
    $form['bail_criteria'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Bail Criteria'),
      '#description' => $this->t('Criteria that prevent adding Override Mode details to DOM elements via drupalSettings. Having no criteria can make for much larger response payloads.'),
      '#states' => [
        'visible' => [
          ':input[name="override_mode"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['memory_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Memory Limit'),
      '#description' => $this->t('The total memory usage limit in MB.'),
      '#default_value' => $config->get('memory_limit'),
      '#group' => 'bail_criteria',
    ];
    $form['variables_max_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Variables Max Size'),
      '#description' => $this->t('The maximum number of nodes in the template variables array.'),
      '#default_value' => $config->get('variables_max_size'),
      '#group' => 'bail_criteria',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('component_library.override_mode.settings');
    if ($form_state->getValue('override_mode') != $config->get('override_mode')) {
      $this->messenger()->addMessage($this->t('Please <a href="@url">clear caches</a> when enabling or disabling Override Mode for the change to take effect.', [
        '@url' => Url::fromRoute('system.performance_settings')->toString(),
      ]));
    }

    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
