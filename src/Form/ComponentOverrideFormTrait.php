<?php

declare(strict_types=1);

namespace Drupal\component_library\Form;

use Drupal\component_library\Entity\ComponentOverride;
use Drupal\Core\Form\FormStateInterface;

/**
 * Component override form trait.
 */
trait ComponentOverrideFormTrait {

  /**
   * Get value.
   *
   * Helper function to resolve a value from either plugin data, the override
   * entity, or the prepareOverrideEvent.
   *
   * @param string $key
   *   The value to retrieve.
   * @param \Drupal\component_library\Entity\ComponentOverride $override
   *   The override entity.
   * @param \Drupal\Core\Form\FormStateInterface|null $form_state
   *   The form state.
   *
   * @return string|null
   *   The value.
   */
  public function getValue(string $key, ComponentOverride $override, ?FormStateInterface $form_state = NULL): ?string {
    $value = NULL;
    $data = $override->getPluginData();
    if ($value === NULL && \array_key_exists($key, $data)) {
      $value = $data[$key];
    }
    if ($value === NULL) {
      $value = $override->get($key);
    }
    if ($value === NULL) {
      $value = $this->prepareOverrideEvent->get($key);
    }
    return $value;
  }

}
