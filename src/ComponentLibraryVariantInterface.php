<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a component library variant entity type.
 */
interface ComponentLibraryVariantInterface extends ConfigEntityInterface {

  /**
   * Returns pattern entity.
   */
  public function getPatternEntity(): ?ComponentLibraryPatternInterface;

}
