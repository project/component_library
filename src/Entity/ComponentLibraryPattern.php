<?php

declare(strict_types=1);

namespace Drupal\component_library\Entity;

use Drupal\Component\Serialization\Yaml;
use Drupal\component_library\ComponentLibraryPatternInterface;
use Drupal\component_library\ComponentLibraryVariantInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the component library pattern entity type.
 *
 * @ConfigEntityType(
 *   id = "component_library_pattern",
 *   label = @Translation("Component library pattern"),
 *   label_collection = @Translation("Library Patterns"),
 *   label_singular = @Translation("library pattern"),
 *   label_plural = @Translation("library patterns"),
 *   label_count = @PluralTranslation(
 *     singular = "@count library pattern",
 *     plural = "@count library patterns",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\component_library\ComponentLibraryPatternListBuilder",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "add" = "Drupal\component_library\Form\ComponentLibraryPatternForm",
 *       "edit" = "Drupal\component_library\Form\ComponentLibraryPatternForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "add_variant" = "\Drupal\component_library\Menu\VariantLocalActionProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *   },
 *   config_prefix = "pattern",
 *   admin_permission = "administer component library patterns",
 *   links = {
 *     "collection" = "/admin/structure/component-library/patterns",
 *     "add-form" = "/admin/structure/component-library/pattern/add",
 *     "edit-form" = "/admin/structure/component-library/pattern/{component_library_pattern}",
 *     "delete-form" = "/admin/structure/component-library/pattern/{component_library_pattern}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "data",
 *     "description",
 *     "tags",
 *   }
 * )
 */
final class ComponentLibraryPattern extends ConfigEntityBase implements ComponentLibraryPatternInterface {

  /**
   * The pattern ID.
   */
  protected string $id;

  /**
   * The pattern label.
   */
  protected string $label;

  /**
   * The pattern description.
   */
  protected string $description;

  /**
   * The preview data.
   */
  protected string $data;

  /**
   * The "tags" of a Pattern.
   */
  protected array $tags = [];

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage): void {
    parent::postCreate($storage);
    self::clearCacheOnSave();
  }

  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);
    self::clearCacheOnSave();
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities): void {
    parent::postDelete($storage, $entities);
    self::clearCacheOnSave();
  }

  /**
   * Clear ui_patterns cache after component library patterns are adjusted.
   */
  private static function clearCacheOnSave(): void {
    if (\Drupal::hasService('plugin.manager.ui_patterns')) {
      \Drupal::service('plugin.manager.ui_patterns')->clearCachedDefinitions();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    return Yaml::decode($this->get('data')) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getVariants(): array {
    return $this->entityTypeManager()
      ->getStorage('component_library_variant')
      ->loadByProperties(['pattern' => $this->id(), 'status' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function getVariant(string $id): ?ComponentLibraryVariantInterface {
    return $this->getVariants()[$id] ?? NULL;
  }

}
