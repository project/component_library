<?php

declare(strict_types=1);

namespace Drupal\component_library\Entity;

use Drupal\component_library\Event\GetAssetFilesEvent;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the component_library_asset entity type.
 *
 * @ConfigEntityType(
 *   id = "component_library_asset",
 *   label = @Translation("Component Library Asset"),
 *   label_collection = @Translation("Component Library Assets"),
 *   label_singular = @Translation("Component Library Asset"),
 *   label_plural = @Translation("Component Library Assets"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Component Library Asset",
 *     plural = "@count Component Library Assets",
 *   ),
 *   handlers = {
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "list_builder" = "Drupal\component_library\ComponentLibraryAssetListBuilder",
 *     "form" = {
 *       "add" = "Drupal\component_library\Form\ComponentLibraryAssetForm",
 *       "edit" = "Drupal\component_library\Form\ComponentLibraryAssetForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "component_library_asset",
 *   admin_permission = "administer component library assets",
 *   links = {
 *     "collection" = "/admin/structure/component-library/assets",
 *     "add-form" = "/admin/structure/component-library/assets/add",
 *     "edit-form" = "/admin/structure/component-library/assets/{component_library_asset}",
 *     "delete-form" = "/admin/structure/component-library/assets/{component_library_asset}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "files",
 *   }
 * )
 */
final class ComponentLibraryAsset extends ConfigEntityBase {

  /**
   * The component_library_asset ID.
   */
  protected $id;

  /**
   * The component_library_asset label.
   */
  protected string $label;

  /**
   * An array of file attributes with the following elements:
   * - 'code': The JS or CSS file contents.
   * - 'name': The file name (without extension).
   * - 'type': The file extension.
   * - 'path': The path to the generated file.
   */
  protected array $files = [];

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities): void {
    $asset_service = \Drupal::service('component_library.asset');
    foreach ($entities as $asset) {
      $asset_service->clearFiles($asset);
    }
  }

  /**
   * Get files.
   *
   * @return array
   *   An array of files and their attributes.
   */
  public function getFiles($create = FALSE): array {
    foreach ($this->files as &$file) {
      $file['path'] = sprintf('public://component_library_assets/%s/%s/%s.%s', $this->id(), $file['type'], $file['name'], $file['type']);
    }

    /** @var \Drupal\component_library\Event\GetAssetFilesEvent $event */
    $event = \Drupal::service('event_dispatcher')->dispatch(new GetAssetFilesEvent($this, $this->files, $create));
    $this->files = $event->getFiles();

    return $this->files;
  }

  /**
   * Set files.
   *
   * @param array $files
   *   An array of files and their attributes.
   */
  public function setFiles(array $files): void {
    $this->files = $files;
  }

}
