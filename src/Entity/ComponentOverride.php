<?php

declare(strict_types=1);

namespace Drupal\component_library\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\component_library\Event\PrepareOverrideEvent;
use Drupal\component_library\Plugin\ComponentOverride\ComponentOverrideInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the component override entity type.
 *
 * @ConfigEntityType(
 *   id = "component_override",
 *   label = @Translation("Component Override"),
 *   label_collection = @Translation("Component Overrides"),
 *   label_singular = @Translation("component override"),
 *   label_plural = @Translation("component overrides"),
 *   label_count = @PluralTranslation(
 *     singular = "@count component override",
 *     plural = "@count component overrides",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\component_library\Form\ComponentOverrideForm",
 *       "edit" = "Drupal\component_library\Form\ComponentOverrideForm",
 *       "delete" = "Drupal\component_library\Form\ComponentOverrideDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\component_library\ComponentOverrideListBuilder",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *   },
 *   config_prefix = "override",
 *   admin_permission = "administer component overrides",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/appearance/settings/{theme}/component-library/override/add",
 *     "edit-form" = "/override/{component_override}/edit",
 *     "delete-form" = "/override/{component_override}/delete",
 *     "collection" = "/admin/appearance/settings/{theme}/component-library/override",
 *   },
 *   field_ui_base_route = "component_library.settings",
 *   config_export = {
 *     "id",
 *     "label",
 *     "override",
 *     "theme",
 *     "plugin",
 *     "plugin_data",
 *     "template",
 *     "variables",
 *     "libraries",
 *   }
 * )
 */
final class ComponentOverride extends ConfigEntityBase {

  /**
   * The override ID.
   */
  protected string $id;

  /**
   * The override label.
   */
  protected string $label;

  /**
   * The theme suggestion/template to override.
   */
  protected string $override;

  /**
   * The theme to which the override applies.
   */
  protected string $theme;

  /**
   * The override plugin type.
   */
  protected string $plugin;

  /**
   * Plugin data.
   */
  protected string $plugin_data;

  /**
   * The template.
   */
  protected string $template;

  /**
   * The variables available to the template.
   */
  protected string $variables;

  /**
   * The libraries attached to the template.
   */
  protected array $libraries = [];

  /**
   * Set variables.
   *
   * @param array $variables
   *   The preprocessed variables.
   */
  public function setVariables(array $variables): void {
    $this->variables = \serialize($variables);
  }

  /**
   * Get variables.
   *
   * @return array|null
   *   The preprocessed variables.
   */
  public function getVariables(): ?array {
    $variables = $this->get('variables');
    if (empty($variables)) {
      return NULL;
    }
    // phpcs:ignore DrupalPractice.FunctionCalls.InsecureUnserialize.InsecureUnserialize
    return \unserialize($variables);
    // phpcs:enable
  }

  /**
   * Get plugin data.
   *
   * Retrieves any plugin form data.
   *
   * @see \Drupal\component_library\Plugin\ComponentOverride\ComponentOverrideBase::submitConfigurationForm
   * @see \Drupal\component_library\Plugin\ComponentOverride\ComponentOverrideBase::getPluginValue()
   *
   * @return array
   */
  public function getPluginData(): array {
    return $this->get('plugin_data') ? Json::decode($this->get('plugin_data')) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);
    self::clearCaches($this);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities): void {
    parent::postDelete($storage, $entities);
    foreach ($entities as $entity) {
      self::clearCaches($entity);
    }
  }

  /**
   * Clear caches.
   *
   * Rebuild the theme registry and give plugins an opportunity to clear caches
   * so that template changes are shown.
   */
  private static function clearCaches(ComponentOverride $override): void {
    \Drupal::service('theme.registry')->reset();
    \Drupal::service('twig')->invalidate();
    \Drupal::service('plugin.manager.component_override')
      ->createInstance($override->get('plugin'), ['override' => $override])
      ->clearCaches($override);
  }

  /**
   * Get the override plugin.
   */
  public function getPlugin(?PrepareOverrideEvent $prepare_override_event = NULL): ?ComponentOverrideInterface {
    $plugin = $this->get('plugin');
    if ($plugin !== NULL) {
      if ($prepare_override_event === NULL) {
        $prepare_override_event = new PrepareOverrideEvent();
      }
      return \Drupal::service('plugin.manager.component_override')
        ->createInstance($plugin, [
          'override' => $this,
          'prepare_override_event' => $prepare_override_event,
        ]);
    }
    return NULL;
  }

}
