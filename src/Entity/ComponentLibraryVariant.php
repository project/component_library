<?php

declare(strict_types=1);

namespace Drupal\component_library\Entity;

use Drupal\component_library\ComponentLibraryVariantInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the component library variant entity class.
 *
 * @ConfigEntityType(
 *   id = "component_library_variant",
 *   label = @Translation("Component Library Variant"),
 *   label_collection = @Translation("Component Library Variants"),
 *   label_singular = @Translation("component variant"),
 *   label_plural = @Translation("component variants"),
 *   label_count = @PluralTranslation(
 *     singular = "@count component variant",
 *     plural = "@count component variants",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\component_library\ComponentLibraryVariantListBuilder",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "add" = "Drupal\component_library\Form\ComponentLibraryVariantForm",
 *       "edit" = "Drupal\component_library\Form\ComponentLibraryVariantForm",
 *       "delete" = "Drupal\component_library\Form\ComponentLibraryVariantDeleteForm",
 *       "duplicate" = "Drupal\component_library\Form\VariantDuplicateForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *       "delete-multiple" = "\Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *   },
 *   config_prefix = "variant",
 *   admin_permission = "administer component library variants",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "collection" = "/admin/structure/component-library/variants",
 *     "add-form" = "/admin/structure/component-library/variant/add",
 *     "canonical" = "/component_library_variants/{component_library_variant}",
 *     "edit-form" = "/admin/structure/component-library/variant/{component_library_variant}/edit",
 *     "delete-form" = "/admin/structure/component-library/variant/{component_library_variant}/delete",
 *     "duplicate-form" = "/admin/structure/component-library/{component_library_variant}/duplicate",
 *     "delete-multiple-form" = "/admin/structure/component-library/variants/delete",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "template",
 *     "description",
 *     "pattern",
 *   },
 * )
 */
final class ComponentLibraryVariant extends ConfigEntityBase implements ComponentLibraryVariantInterface {

  /**
   * The library variant ID.
   */
  protected ?string $id;

  /**
   * The library variant label.
   */
  protected string $label;

  /**
   * The template.
   */
  protected string $template;

  /**
   * Component pattern ID.
   */
  protected ?string $pattern = NULL;

  /**
   * {@inheritdoc}
   */
  public function getPatternEntity(): ?ComponentLibraryPattern {
    return $this->pattern ? ComponentLibraryPattern::load($this->pattern) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    if ($pattern = $this->getPatternEntity()) {
      $this->addDependency('config', $pattern->getConfigDependencyName());
    }
    return $this;
  }

}
