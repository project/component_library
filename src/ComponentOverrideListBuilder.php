<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of component_override entities.
 *
 * @see \Drupal\block_content\Entity\BlockContentType
 */
final class ComponentOverrideListBuilder extends ConfigEntityListBuilder {

  /**
   * The current route match service.
   */
  private CurrentRouteMatch $currentRouteMatch;

  /**
   * Constructs a new FilterFormatListBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The current route match service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, CurrentRouteMatch $route_match) {
    parent::__construct($entity_type, $storage);

    $this->currentRouteMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    return new self(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_route_match'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['entity_type'] = $this->t('Entity Type');
    $header['template_suggestion'] = $this->t('Template suggestion');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row = [];
    $route_theme = $this->currentRouteMatch->getParameter('theme');
    if ($route_theme !== NULL || $route_theme === $entity->get('theme')) {
      $row['label'] = $entity->toLink(NULL, 'edit-form')->toString();
      $row['entity_type'] = $entity->get('plugin');
      $row['template_suggestion'] = $entity->get('override');
      $row += parent::buildRow($entity);
    }
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTitle(): TranslatableMarkup {
    return $this->t('Component overrides list');
  }

}
