<?php

declare(strict_types=1);

namespace Drupal\component_library\Plugin\ComponentOverride;

use Drupal\component_library\Entity\ComponentOverride;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin for Override Mode.
 *
 * @see override_mode.js
 *
 * @ComponentOverride(
 *   id = "override_mode",
 *   label = "Override Mode",
 * )
 */
final class OverrideMode extends ComponentOverrideBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $plugin_id = $this->getPluginValue('plugin', $this->override, $form_state);
    if ($plugin_id) {
      $default_value = $this->getPluginValue('override', $this->override, $form_state);
      // Get override options from URL.
      $override_options = $this->prepareOverrideEvent->getOverrideOptions();
      if (empty($override_options)) {
        $override_options = [$default_value];
      }
      $form['override'] = [
        '#type' => 'select',
        '#title' => $this->t('Override'),
        '#options' => \array_combine($override_options, $override_options),
        '#empty_option' => $this->t('- Select -'),
        '#empty_value' => '',
        '#required' => TRUE,
        '#default_value' => $default_value,
        '#disabled' => !$this->override->isNew(),
      ];
    }
    else {
      $form['override']['#type'] = 'hidden';
    }
    $form['base_hook'] = [
      '#type' => 'value',
      '#value' => $this->getPluginValue('base_hook', $this->override, $form_state),
    ];
    $form['cache'] = [
      '#type' => 'value',
      '#value' => $this->getPluginValue('cache', $this->override, $form_state),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function clearCaches(ComponentOverride $override): void {
    Cache::invalidateTags([$this->getPluginValue('cache', $this->override)]);
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseHook(): string {
    return $this->getPluginValue('base_hook', $this->override);
  }

}
