<?php

declare(strict_types=1);

namespace Drupal\component_library\Plugin\ComponentOverride;

use Drupal\component_library\Entity\ComponentOverride;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

interface ComponentOverrideInterface extends PluginFormInterface {

  /**
   * Build configuration form.
   *
   * @param array $form
   *   An associative array containing the initial structure of the plugin form.
   *   The form needs to provide an "override" form element which should allow
   *   the user to select a theme registry to override.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. Calling code should pass on a subform
   *   state created through
   *   \Drupal\Core\Form\SubformState::createForSubform().
   *
   * @return array
   *   The form structure.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array;

  /**
   * Get plugin value.
   *
   * Gets the most relevant plugin value from either the override or the
   * current form state and ensures form state is current.
   *
   * @param $key
   *   The Component Override property or plugin data to retrieve.
   * @param \Drupal\component_library\Entity\ComponentOverride $override
   *   The component override.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   The current plugin value for the property.
   */
  public function getPluginValue($key, ComponentOverride $override, FormStateInterface $form_state): ?string;

  /**
   * Clear caches.
   *
   * Gives plugins an opportunity to clear any relevant caches so that the
   * template changes are shown immediately.
   *
   * @param \Drupal\component_library\Entity\ComponentOverride $override
   *   The component override.
   */
  public function clearCaches(ComponentOverride $override): void;

  /**
   * Get base hook.
   *
   * @return string
   *   The theme base hook.
   */
  public function getBaseHook(): string;

}
