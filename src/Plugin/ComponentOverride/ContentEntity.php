<?php

declare(strict_types=1);

namespace Drupal\component_library\Plugin\ComponentOverride;

use Drupal\component_library\Entity\ComponentOverride;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ComponentOverride(
 *   id = "content_entity",
 *   deriver = "Drupal\component_library\Plugin\Deriver\ContentEntity"
 * )
 */
final class ContentEntity extends ComponentOverrideBase {

  private EntityTypeBundleInfoInterface $bundleInfo;
  private EntityDisplayRepositoryInterface $entityDisplay;
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->bundleInfo = $container->get('entity_type.bundle.info');
    $instance->entityDisplay = $container->get('entity_display.repository');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $plugin_id = $this->getPluginValue('plugin', $this->override, $form_state);
    if ($plugin_id) {
      $bundles = [];
      $entity_type = $this->getBaseHook();
      foreach ($this->bundleInfo->getBundleInfo($entity_type) as $type => $label) {
        $bundles[$type] = $label['label'];
      }
      $bundle = $this->getPluginValue('bundle', $this->override, $form_state);
      if (\count($bundles) > 1) {
        $form['bundle'] = [
          '#type' => 'select',
          '#title' => $this->t('Bundle'),
          '#options' => $bundles,
          '#empty_option' => $this->t('- Select -'),
          '#empty_value' => '',
          '#required' => TRUE,
          '#default_value' => $bundle,
          '#disabled' => !$this->override->isNew(),
          '#ajax' => [
            'callback' => [$this, 'loadPluginContainer'],
            'wrapper' => 'override-plugin-container',
            'effect' => 'fade',
          ],
        ];
      }
      else {
        $bundle = \array_key_first($bundles);
        $form['bundle'] = [
          '#type' => 'value',
          '#value' => $bundle,
        ];
        $form_state->setValue('bundle', $bundle);
      }
      $view_mode = $this->getPluginValue('view_mode', $this->override, $form_state);
      if ($bundle) {
        $form['view_mode'] = [
          '#type' => 'select',
          '#title' => $this->t('View Mode'),
          '#options' => $this->entityDisplay->getViewModeOptionsByBundle($entity_type, $bundle),
          '#empty_option' => $this->t('- Select -'),
          '#empty_value' => '',
          '#required' => TRUE,
          '#default_value' => $view_mode,
          '#disabled' => !$this->override->isNew(),
          '#ajax' => [
            'callback' => [$this, 'loadPluginContainer'],
            'wrapper' => 'override-plugin-container',
            'effect' => 'fade',
          ],
        ];
      }

      if ($bundle && $view_mode) {
        $this->prepareOverrideEvent->setPluginData([
          'bundle' => $bundle,
          'view_mode' => $view_mode,
        ]);
        $this->prepareOverrideEvent->setEntity($this->override);
        $this->prepareOverrideEvent = $this->dispatcher->dispatch($this->prepareOverrideEvent);
        $values = $this->prepareOverrideEvent->getOverrideOptions();
        $form['override'] = [
          '#type' => 'select',
          '#title' => $this->t('Override'),
          '#options' => \array_combine($values, $values),
          '#empty_option' => $this->t('- Select -'),
          '#empty_value' => '',
          '#required' => TRUE,
          '#default_value' => $this->getPluginValue('override', $this->override, $form_state),
          '#disabled' => !$this->override->isNew(),
        ];
      }
      else {
        $form['override']['#type'] = 'hidden';
      }
    }

    return $form;
  }

  /**
   * AJAX callback to load the override select list.
   */
  public function loadPluginContainer(array $form, FormStateInterface $form_state): array {
    return $form['plugin_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function clearCaches(ComponentOverride $override): void {
    // Invalidate entity cache tags.
    $entity_type = $this->getBaseHook();
    $bundle = $this->getPluginValue('bundle', $override, new FormState());
    $bundle_key = $this->entityTypeManager->getDefinition($entity_type)->getKey('bundle');
    $entities = $this->entityTypeManager->getStorage($entity_type)->loadByProperties([
      $bundle_key => $bundle,
    ]);
    foreach ($entities as $entity) {
      Cache::invalidateTags($entity->getCacheTagsToInvalidate());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseHook(): string {
    return \explode(':', $this->override->get('plugin'))[1];
  }

}
