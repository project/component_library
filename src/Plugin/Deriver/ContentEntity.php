<?php

declare(strict_types=1);

namespace Drupal\component_library\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class ContentEntity extends DeriverBase implements ContainerDeriverInterface {

  private EntityTypeManagerInterface $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): self {
    return new self($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    foreach ($this->entityTypeManager->getDefinitions() as $type => $definition) {
      if ($definition instanceof ContentEntityType && $definition->hasViewBuilderClass() && $definition->getViewBuilderClass() !== EntityViewBuilder::class && \function_exists("{$type}_theme_suggestions_{$type}")) {
        $this->derivatives[$type] = $base_plugin_definition;
        $this->derivatives[$type]['id'] = $definition->id();
        $this->derivatives[$type]['label'] = $definition->getLabel();
      }
    }
    return $this->derivatives;
  }

}
