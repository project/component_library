<?php

declare(strict_types=1);

namespace Drupal\component_library\Plugin\Deriver;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\ui_patterns\Definition\PatternDefinition;
use Drupal\ui_patterns\Plugin\Deriver\AbstractPatternsDeriver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Component pattern deriver.
 */
final class ComponentPatternDeriver extends AbstractPatternsDeriver {

  private EntityTypeManagerInterface $entityTypeManager;
  private ExtensionList $moduleList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): self {
    $deriver = parent::create($container, $base_plugin_id);
    /* @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $deriver->entityTypeManager = $container->get('entity_type.manager');
    $deriver->moduleList = $container->get('extension.list.module');
    return $deriver;
  }

  /**
   * {@inheritdoc}
   */
  public function getPatterns(): array {
    $patterns = [];

    $pattern_storage = $this->entityTypeManager->getStorage('component_library_pattern');
    /** @var \Drupal\component_library\ComponentLibraryPatternInterface[] $component_patterns */
    $component_patterns = $pattern_storage->loadMultiple();

    $path = $this->moduleList->getPath('component_library');
    foreach ($component_patterns as $component_pattern) {
      $fields = [];
      foreach ($component_pattern->getFields() as $key => $value) {
        $fields[$key] = [
          'label' => $key,
          'preview' => $value,
        ];
      }
      $definition = [
        'id' => $component_pattern->id(),
        'label' => $component_pattern->label(),
        'description' => $component_pattern->get('description'),
        'base path' => $path,
        'theme hook' => 'component_library_component__' . $component_pattern->id(),
        'template' => 'component_library_component',
        'provider' => 'component_library',
        'file name' => 'component_library',
        'fields' => $fields,
      ];

      foreach ($component_pattern->getVariants() as $component_variant) {
        $definition['variants'][$component_variant->id()] = [
          'label' => $component_variant->label(),
          'description' => '',
        ];
      }

      $patterns[] = new PatternDefinition($definition);

    }

    return $patterns;
  }

}
