<?php

declare(strict_types=1);

namespace Drupal\component_library\Plugin\UiPatterns\Pattern;

use Drupal\Core\Extension\ExtensionList;
use Drupal\ui_patterns\Plugin\PatternBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Component pattern plugin.
 *
 * @UiPattern(
 *   id = "component_library_component",
 *   deriver = "\Drupal\component_library\Plugin\Deriver\ComponentPatternDeriver"
 * )
 */
final class ComponentPattern extends PatternBase {

  private ExtensionList $moduleList;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->moduleList = $container->get('extension.list.module');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getThemeImplementation(): array {
    $items = parent::getThemeImplementation();
    $definition = $this->getPluginDefinition();

    $template_path = $this->moduleList->getPath('component_library') . '/templates';
    foreach ($items as $item_id => $item) {
      $items[$item_id]['path'] = $template_path;
      $items[$item_id]['template'] = 'component-library-component';
      $items[$item_id]['base hook'] = 'component_library_component';
      $items[$item_id]['component_id'] = $definition->id();
    }
    return $items;
  }

}
