<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a component library pattern entity type.
 */
interface ComponentLibraryPatternInterface extends ConfigEntityInterface {

  /**
   * Returns available component fields (slots) with default values.
   */
  public function getFields(): array;

  /**
   * Gets pattern variations.
   *
   * @return \Drupal\component_library\ComponentLibraryVariantInterface[]
   *   Array of pattern variations.
   */
  public function getVariants(): array;

  /**
   * Gets pattern variation.
   */
  public function getVariant(string $id): ?ComponentLibraryVariantInterface;

}
