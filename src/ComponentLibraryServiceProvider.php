<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\component_library\EventSubscriber\EntityYamlPlaceholders;
use Drupal\component_library\Twig\WorkspaceAwareLoader;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Defines a service provider for the Component Library module.
 */
final class ComponentLibraryServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $modules = $container->getParameter('container.modules');
    if (\array_key_exists('layout_builder', $modules)) {
      $container->register('component_library.entity_yaml_placeholder.event_subscriber', EntityYamlPlaceholders::class)
        ->addTag('event_subscriber')
        ->addArgument(new Reference('layout_builder.sample_entity_generator'))
        ->addArgument(new Reference('logger.channel.component_library'));
    }
  }

}
