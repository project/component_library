<?php

declare(strict_types=1);

namespace Drupal\component_library\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Not admin route subscriber.
 */
final class NotAdminRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // Use the active theme when overriding components to have access to the
    // its theme registry.
    if ($route = $collection->get('entity.component_override.add_form')) {
      $route->setOption('_admin_route', FALSE);
    }
    if ($route = $collection->get('entity.component_override.edit_form')) {
      $route->setOption('_admin_route', FALSE);
    }
  }

}
