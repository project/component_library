<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of component library patterns.
 */
final class ComponentLibraryPatternListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['tags'] = $this->t('Tags');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row = [];
    /** @var \Drupal\component_library\ComponentLibraryPatternInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['tags'] = Tags::implode($entity->get('tags'));
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['#attached']['library'][] = 'component_library/pattern_listing';

    $title = $this->t('Filter by pattern label, machine name or tags');
    $build['filter'] = [
      '#type' => 'search',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#size' => 60,
      '#placeholder' => $title,
      '#attributes' => [
        'data-drupal-selector' => 'component-library-pattern-listing-filter',
        'autocomplete' => 'off',
      ],
      '#weight' => -10,
    ];

    $build['table']['#attributes']['data-drupal-selector'] = 'component-library-pattern-listing-table';

    return $build;
  }

}
