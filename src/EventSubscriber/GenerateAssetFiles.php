<?php

declare(strict_types=1);

namespace Drupal\component_library\EventSubscriber;

use Drupal\component_library\Asset;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\component_library\Entity\ComponentLibraryAsset;
use Drupal\Core\Entity\Annotation\EntityType;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Regenerates library assets on config import.
 */
final class GenerateAssetFiles implements EventSubscriberInterface {

  const CONFIG_NAMESPACE = 'component_library.component_library_asset';

  private Asset $asset;
  private ConfigFactoryInterface $configFactory;
  private EntityTypeManagerInterface $entityTypeManager;

  public function __construct(ConfigFactoryInterface $config_factory, Asset $asset, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->asset = $asset;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[ConfigEvents::SAVE][] = ['onConfigSave'];
    $events[ConfigEvents::IMPORT][] = ['onConfigImport'];
    return $events;
  }

  /**
   * The config save event listener.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The config crud event.
   */
  public function onConfigSave(ConfigCrudEvent $event): void {
    $config = $event->getConfig();
    if ($this->isAssetConfig($config->getName())) {
      $this->generateFiles($config->getRawData()['id']);
    }
  }

  /**
   * Generates library asset files on config import.
   *
   * @param \Drupal\Core\Config\ConfigImporterEvent $event
   *   The config import event.
   */
  public function onConfigImport(ConfigImporterEvent $event): void {
    $changes = $event->getChangelist();
    $configs = \array_merge($changes['create'], $changes['update'], $changes['rename']);

    foreach ($configs as $config_name) {
      if ($this->isAssetConfig($config_name)) {
        $asset_id = $this->configFactory->get($config_name)->getRawData()['id'];
        /** @var \Drupal\component_library\Entity\ComponentLibraryAsset $asset */
        $asset = $this->entityTypeManager->getStorage('component_library_asset')
          ->load($asset_id);
        $this->asset->clearFiles($asset);
        $this->generateFiles($asset_id);
      }
    }
  }

  /**
   * Generate CSS and JS files.
   *
   * @param string $config_name
   *   The ComponentLibraryAsset ID.
   */
  private function generateFiles(string $config_name): void {
    $asset = ComponentLibraryAsset::load($config_name);
    $this->asset->clearFiles($asset);
    $this->asset->generateLibraryFiles($asset);
  }

  /**
   * Is not asset config.
   *
   * @param string $config_name
   *   The config name.
   *
   * @return bool
   *   Whether or not the config is a ComponentLibraryAsset.
   */
  private function isAssetConfig(string $config_name): bool {
    return \str_starts_with($config_name, self::CONFIG_NAMESPACE);
  }

}
