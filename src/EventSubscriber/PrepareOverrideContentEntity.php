<?php

declare(strict_types=1);

namespace Drupal\component_library\EventSubscriber;

use Drupal\component_library\ComponentOverrideManager;
use Drupal\component_library\Event\PrepareOverrideEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\layout_builder\Entity\SampleEntityGeneratorInterface;

/**
 * Prepare override content entity.
 */
final class PrepareOverrideContentEntity extends PrepareOverrideBase {

  private SampleEntityGeneratorInterface $generator;
  private ComponentOverrideManager $manager;
  private EntityTypeManagerInterface $entityTypeManager;
  private ModuleHandlerInterface $moduleHandler;

  public function __construct(SampleEntityGeneratorInterface $entityGenerator, ComponentOverrideManager $manager, EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler) {
    $this->generator = $entityGenerator;
    $this->manager = $manager;
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * On prepare override.
   *
   * @param \Drupal\component_library\Event\PrepareOverrideEvent $event
   *   The prepare override event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function onPrepareOverride(PrepareOverrideEvent $event): void {
    $plugin = $event->getPlugin();
    $plugin_data = $event->getPluginData();
    if ($plugin === NULL || $plugin_data === NULL) {
      return;
    }
    if (!\array_key_exists('bundle', $plugin_data) || !\str_starts_with($plugin, 'content_entity:')) {
      return;
    }
    // Generate an entity.
    $view_mode = $plugin_data['view_mode'] ?? 'default';
    $definition = $this->manager->getDefinition($plugin);
    $entity = $this->generator->get($definition['id'], $plugin_data['bundle']);
    $view_builder = $this->entityTypeManager->getViewBuilder($definition['id']);
    $variables = ['elements' => $view_builder->view($entity, $view_mode)];

    // Get override options.
    $entity_type_id = \str_replace('content_entity:', '', $plugin);
    $suggestions = $this->moduleHandler->invokeAll("theme_suggestions_$entity_type_id", [$variables]);
    $suggestions = \array_filter($suggestions, static fn ($value) => !\str_ends_with($value, '__') && !\str_contains($value, '____'));
    $event->setOverrideOptions($suggestions);

    // Run preprocess functions.
    $override = $event->getEntity();
    if (!$override->get('override')) {
      return;
    }
    $definition = $this->manager->getDefinition($plugin);
    $registry = $this->manager->getThemeRegistry();
    $this->manager->ensureThemeRegistry($override, $registry);
    $info = $registry[$override->get('override')];
    $hook = $definition['id'];

    if (\array_key_exists('preprocess functions', $info)) {
      foreach ($info['preprocess functions'] as $preprocessor_function) {
        if (\function_exists($preprocessor_function)) {
          $preprocessor_function($variables, $hook, $info);
        }
      }
    }
    $event->setVariables($variables);
    $this->overridePrepared();
  }

}
