<?php

declare(strict_types=1);

namespace Drupal\component_library\EventSubscriber;

use Drupal\component_library\Event\AddVariablesPlaceholderEvent;
use Drupal\component_library\Event\ReplaceVariablesPlaceholderEvent;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Override mode config entity variable placeholders.
 */
final class VariablesPlaceholderConfigEntity implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[AddVariablesPlaceholderEvent::class][] = ['onAddVariablesPlaceholder'];
    $events[ReplaceVariablesPlaceholderEvent::class][] = ['onReplaceVariablesPlaceholderEvent'];
    return $events;
  }

  public function onAddVariablesPlaceholder(AddVariablesPlaceholderEvent $event): void {
    $item = $event->getItem();
    if ($item instanceof ConfigEntityInterface) {
      $event->setPlaceholder([
        '#override_mode' => [
          '#type' => 'override_config_entity',
          '#id' => $item->id(),
          '#entity_type' => $item->getEntityTypeId(),
        ],
      ]);
    }
  }

  public function onReplaceVariablesPlaceholderEvent(ReplaceVariablesPlaceholderEvent $event): void {
    $config = $event->getConfig();
    if (isset($config['#type']) && $config['#type'] == 'override_config_entity') {
      $entity = \Drupal::entityTypeManager()->getStorage($config['#entity_type'])->load($config['#id']);
      $event->setReplacement($entity);
    }
  }

}
