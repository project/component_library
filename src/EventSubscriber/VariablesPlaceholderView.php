<?php

declare(strict_types=1);

namespace Drupal\component_library\EventSubscriber;

use Drupal\component_library\Event\AddVariablesPlaceholderEvent;
use Drupal\component_library\Event\ReplaceVariablesPlaceholderEvent;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Override Mode View variable placeholders.
 */
final class VariablesPlaceholderView implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[AddVariablesPlaceholderEvent::class][] = ['onAddVariablesPlaceholder'];
    $events[ReplaceVariablesPlaceholderEvent::class][] = ['onReplaceVariablesPlaceholderEvent'];
    return $events;
  }

  public function onAddVariablesPlaceholder(AddVariablesPlaceholderEvent $event): void {
    $item = $event->getItem();
    // View objects.
    if ($item instanceof ViewExecutable) {
      $event->setPlaceholder([
        '#override_mode' => [
          '#type' => 'override_view_executable',
          '#name' => $item->id(),
          '#display_id' => $item->current_display,
          '#args' => $item->args,
        ],
      ]);
    }
    // View render arrays.
    if (\is_array($item) && isset($item['#type']) && $item['#type'] === 'view') {
      $name = $item['#name'] ?? NULL;
      $display_id = $item['#display_id'] ?? NULL;
      if ($name !== NULL && $display_id !== NULL) {
        $event->setPlaceholder([
          '#override_mode' => [
            '#type' => 'override_view_array',
            '#name' => $name,
            '#display_id' => $display_id,
            '#args' => $item['#arguments'] ?? [],
          ],
        ]);
      }
    }
  }

  public function onReplaceVariablesPlaceholderEvent(ReplaceVariablesPlaceholderEvent $event): void {
    $config = $event->getConfig();
    // View objects.
    if (isset($config['#type']) && $config['#type'] === 'override_view_executable') {
      $view = Views::getView($config['#name']);
      $view->setArguments($config['#args']);
      $view->setDisplay($config['#display_id']);
      $view->initDisplay();
      $view->preExecute();
      $view->execute();
      $event->setReplacement($view);
    }

    // View render arrays.
    if (isset($config['#type']) && $config['#type'] === 'override_view_array') {
      $view = \views_embed_view($config['#name'], $config['#display_id'], ...$config['#args']);
      $event->setReplacement($view);
    }
  }

}
