<?php

declare(strict_types=1);

namespace Drupal\component_library\EventSubscriber;

use Drupal\component_library\Event\OverrideIgnoreTemplateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Ignore template.
 */
final class IgnoreTemplate implements EventSubscriberInterface {

  /**
   * Ignore original theme hooks.
   *
   * @var array|string[]
   *   An array of original theme hooks to ignore.
   */
  private array $ignoredOriginalThemeHooks = [
    'views_view_field',
    'rdf_metadata',
    'region',
    'toolbar',
    'html',
  ];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[OverrideIgnoreTemplateEvent::class][] = ['onIgnore'];
    return $events;
  }

  /**
   * Ignore obvious templates like the toolbar and regions.
   *
   * @param \Drupal\component_library\Event\OverrideIgnoreTemplateEvent $event
   *   The event.
   */
  public function onIgnore(OverrideIgnoreTemplateEvent $event): void {
    $variables = $event->getVariables();

    $theme_hook_original = $variables['theme_hook_original'] ?? NULL;
    if ($theme_hook_original && \in_array($theme_hook_original, $this->ignoredOriginalThemeHooks, TRUE)) {
      $event->ignore();
    }
  }

}
