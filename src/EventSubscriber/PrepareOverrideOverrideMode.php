<?php

declare(strict_types=1);


namespace Drupal\component_library\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\component_library\Event\PrepareOverrideEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Prepare Override Override mode.
 *
 * Used to fill out the override form from URL values for override_mode plugins.
 *
 * @see override_mode.js
 */
final class PrepareOverrideOverrideMode extends PrepareOverrideBase {

  private ?Request $request;

  public function __construct(RequestStack $requestStack) {
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * Populate Override.
   *
   * Used to fill out the override form from URL values.
   *
   * @param \Drupal\component_library\Event\PrepareOverrideEvent $event
   *   The prepare override event.
   *
   * @see override_mode.js
   */
  public function onPrepareOverride(PrepareOverrideEvent $event): void {
    $override_options = $this->request->query->all('override_options') ?? [];
    if ($override_options) {
      $event->setOverrideOptions($override_options);
    }
    $override = $this->request->query->get('theme_suggestion');
    if ($override) {
      $event->setOverride($override);
      $event->prepopulated();
    }
    $variables = $this->request->get('variables');
    if ($variables) {
      $event->setVariables(Json::decode($variables));
    }
    $plugin = $this->request->query->get('plugin_value');
    if ($plugin) {
      $event->setPlugin($plugin);
    }
    $plugin_data = [];
    $base_hook = $this->request->query->get('base_hook');
    if ($base_hook) {
      $plugin_data['base_hook'] = $base_hook;
    }
    $cache = $this->request->query->get('cache');
    if ($cache) {
      $plugin_data['cache'] = $cache;
    }
    if ($plugin_data) {
      $event->setPluginData($plugin_data);
    }
    $this->overridePrepared();
  }

}
