<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\component_library\Annotation\ComponentOverride as ComponentOverrideAnnotation;
use Drupal\component_library\Entity\ComponentOverride;
use Drupal\component_library\Plugin\ComponentOverride\ComponentOverrideInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Theme\Registry;

final class ComponentOverrideManager extends DefaultPluginManager {

  private ThemeExtensionList $extensionList;
  private Registry $themeRegistry;
  private array $registry = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, Registry $theme_registry, ThemeExtensionList $extension_list) {
    parent::__construct('Plugin/ComponentOverride', $namespaces, $module_handler, ComponentOverrideInterface::class, ComponentOverrideAnnotation::class);
    $this->alterInfo('component_override_info');
    $this->setCacheBackend($cache_backend, 'component_override_info_plugins');
    $this->themeRegistry = $theme_registry;
    $this->extensionList = $extension_list;
  }

  /**
   * Get plugin options.
   *
   * @return array
   */
  public function getPluginOptions(): array {
    $options = [];
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      $options[$plugin_id] = $definition['label'];
    }

    return $options;
  }

  /**
   * Get the theme registry.
   *
   * @return array
   *   The theme registry.
   */
  public function getThemeRegistry(): array {
    if ($this->registry === []) {
      $this->registry = $this->themeRegistry->get();
    }
    return $this->registry;
  }

  /**
   * Ensure theme registry.
   *
   * Our twig loader looks at the template name to see if there is an override
   *  for it. Add a theme registry item if one doesn't already exist.
   *
   * @param \Drupal\component_library\Entity\ComponentOverride $override
   *   The override that possibly needs a theme suggestion.
   * @param array $theme_registry
   *   The complete theme registry.
   */
  public function ensureThemeRegistry(ComponentOverride $override, array &$theme_registry): void {
    $override_registry_name = $override->get('override');
    // Ensure there is a theme_registry that our twig loader will pick up.
    if (!\array_key_exists($override_registry_name, $theme_registry)) {
      $base_hook = $override->getPlugin()->getBaseHook();
      $theme_registry[$override_registry_name] = [
        'template' => \str_replace('_', '-', $override_registry_name),
        'theme path' => $this->extensionList->getPath($override->get('theme')),
        'render element' => 'elements',
        'base hook' => $base_hook,
        'type' => 'theme_engine',
        'path' => 'component_override',
      ];
      // Use the base hook information as defaults.
      if (\array_key_exists($base_hook, $theme_registry)) {
        if (!empty($theme_registry[$base_hook]['preprocess functions'])) {
          $theme_registry[$override_registry_name]['preprocess functions'] = $theme_registry[$base_hook]['preprocess functions'];
        }
        $theme_registry[$override_registry_name]['path'] = $theme_registry[$base_hook]['path'];
      }
    }
  }

}
