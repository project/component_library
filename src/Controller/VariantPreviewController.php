<?php

declare(strict_types=1);

namespace Drupal\component_library\Controller;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Component Library routes.
 */
final class VariantPreviewController extends ControllerBase {

  private PrivateTempStore $tempStore;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore service.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory) {
    $this->tempStore = $temp_store_factory->get('component_library');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('tempstore.private'),
    );
  }

  /**
   * Builds the response.
   */
  public function build(): array {
    return [
      '#title' => $this->title(),
      '#type' => 'page',
      'content' => [
        '#type' => 'item',
        '#markup' => $this->tempStore->get('variant_preview'),
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  public function title(): MarkupInterface {
    return $this->t('Preview');
  }

}
