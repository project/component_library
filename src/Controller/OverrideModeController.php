<?php

declare(strict_types=1);

namespace Drupal\component_library\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Override Mode system settings tray.
 */
final class OverrideModeController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function tray(): array {
    // The button_container is populated with "Add override" buttons.
    // @see override_mode.js
    return [
      'tray' => [
        '#type' => 'container',
        'button_container' => [
          '#type' => 'container',
          '#attributes' => [
            'id' => 'override-mode-tray',
          ],
        ],
      ],
    ];
  }

}
