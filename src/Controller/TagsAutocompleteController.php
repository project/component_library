<?php

declare(strict_types=1);

namespace Drupal\component_library\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Returns responses for Component Library autocomplete routes.
 */
final class TagsAutocompleteController extends ControllerBase {

  /**
   * Callback for Pattern tags autocompletion.
   *
   * Like other autocomplete functions, this function inspects the 'q' query
   * parameter for the string to use to search for suggestions.
   */
  public function autocompleteTags(Request $request): JsonResponse {

    $query = $request->query->get('q');
    if (!\is_string($query)) {
      throw new BadRequestHttpException();
    }
    $patterns = $this->entityTypeManager()->getStorage('component_library_pattern')->loadMultiple();

    $matches = [];
    // Keep track of previously processed tags so they can be skipped.
    $processed_tags = [];
    foreach ($patterns as $pattern) {
      foreach ($pattern->get('tags') as $tag) {
        if (\in_array($tag, $processed_tags)) {
          continue;
        }
        $processed_tags[] = $tag;
        if (\mb_stripos($tag, $query) === FALSE) {
          continue;
        }
        $matches[] = ['value' => $tag, 'label' => Html::escape($tag)];
        if (\count($matches) >= 10) {
          break 2;
        }
      }
    }

    return new JsonResponse($matches);
  }

}
