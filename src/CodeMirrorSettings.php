<?php

declare(strict_types=1);

namespace Drupal\component_library;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

class CodeMirrorSettings implements ContainerInjectionInterface {

  private ConfigFactory $configFactory;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Enable CSS, JS, and TWIG CodeMirror support.
   */
  public function enableLanguages() {
    $codemirror_settings = $this->configFactory->getEditable('codemirror_editor.settings');
    $data = $codemirror_settings->getRawData();
    $languages = &$data['language_modes'];
    foreach (['twig', 'css', 'javascript'] as $language) {
      if (!in_array($language, $languages, TRUE)) {
        $languages[] = $language;
      }
    }
    $codemirror_settings->setData($data);
    $codemirror_settings->save();
  }

}
