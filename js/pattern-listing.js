/**
 * @file
 * Component Library pattern listing.
 */

(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.componentLibraryPatternListing = {
    attach: function (context) {

      const input = once('componentLibrary.patternListingFilter', 'input[data-drupal-selector="component-library-pattern-listing-filter"]', context).shift();
      if (!input) {
        return;
      }

      const rows = context.querySelectorAll('table[data-drupal-selector="component-library-pattern-listing-table"] tbody tr')

      // Index data.
      const index = new Map();
      rows.forEach(row => {
        const cells = row.querySelectorAll('td');
        const data = [];
        // Label and machine name.
        data.push(cells[0].innerHTML.toLowerCase(), cells[1].innerHTML)
        // Tags.
        if (cells[2].innerHTML !== '') {
          data.push(...cells[2].innerHTML.toLowerCase().split(', '))
        }
        index.set(row, data);
      });

      input.addEventListener('keyup', () => {
        const query = input.value.toLowerCase();
        const toggler = query.length > 0 ?
          row => row.hidden = index.get(row).filter(item => item.includes(query)).length === 0:
          row => row.hidden = false;
        rows.forEach(toggler);
      })

    }
  };

} (Drupal, once));
